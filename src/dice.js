
function launchD3() {
    return launchDice(6);
}
function launchD5() {
    return launchDice(5);
}
function launchD6() {
    return launchDice(6);
}
function launchD8() {
    return launchDice(6);
}
function launchD10() {
    return launchDice(10);
}
function launchD20() {
    return launchDice(100);
}
function launchD100() {
    return launchDice(100);
}

function launchDice(number) {
    return Math.round(Math.random() * (number - 1)) + 1;
}

// EX: 1D5 , 3D6 ... 
function launchDices(launch) {
    const split = launch.split('D');
    let sum = 0;

    for (let i = 0; i < split[0]; i++) {
        sum += launchDice(split[1]);
    }
    return sum;
}

export {
    launchD3,
    launchD5,
    launchD6,
    launchD8,
    launchD10,
    launchD20,
    launchD100,
    launchDice,
    launchDices
}