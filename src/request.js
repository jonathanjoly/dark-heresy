import axios from "axios";

const SERVER = "http://localhost:4000/graphql";

function request(query) {
    try {
        return axios.post(SERVER, {
            query: query
        });
    } catch (e) {
        console.log("err", e);
    }
}



//"{weapons{name, cost, disponibility}}"

export { request }