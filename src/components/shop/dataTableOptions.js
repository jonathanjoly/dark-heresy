export default {
  headers: [
    {
      text: 'Objets désiré',
      align: 'center',
      sortable: false,
      value: 'name'
    },
    {
      text: 'Prix de base',
      sortable: false,
      align: 'center',
      value: 'price'
    },
    {
      text: 'Qualité',
      sortable: false,
      align: 'center',
      value: 'quality'
    },
    {
      text: 'Test',
      sortable: false,
      align: 'center',
      value: 'score'
    },
    {
      text: 'Temps requis',
      sortable: false,
      align: 'center',
      value: 'time'
    },
    {
      text: 'Prix à payer',
      sortable: false,
      align: 'center',
      value: 'total'
    },
    {
      text: 'Réussite',
      sortable: false,
      align: 'center',
      value: 'success'
    },

  ]
}