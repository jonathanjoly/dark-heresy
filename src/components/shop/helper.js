import { launchDices, launchD100 } from '../../dice';
import { findId } from '../helper';

const dispoPopulation = {
  'POPU_MOMI': {
    'DISP_BANA': 'DIFF_FACI',
    'DISP_TRCO': 'DIFF_ASFA',
    'DISP_COUR': 'DIFF_MOYE',
    'DISP_ASCO': 'DIFF_ASDI',
    'DISP_INHA': 'DIFF_DIFF',
    'DISP_RARE': 'DIFF_TRDI',
    'DISP_TRRA': 'DIFF_CHMJ',
  },
  'POPU_MODM': {
    'DISP_BANA': 'DIFF_TRFA',
    'DISP_TRCO': 'DIFF_FACI',
    'DISP_COUR': 'DIFF_ASFA',
    'DISP_ASCO': 'DIFF_MOYE',
    'DISP_INHA': 'DIFF_ASDI',
    'DISP_RARE': 'DIFF_DIFF',
    'DISP_TRRA': 'DIFF_TRDI',
  },
  'POPU_MOCM': {
    'DISP_BANA': 'DIFF_REAU',
    'DISP_TRCO': 'DIFF_TRFA',
    'DISP_COUR': 'DIFF_FACI',
    'DISP_ASCO': 'DIFF_ASFA',
    'DISP_INHA': 'DIFF_MOYE',
    'DISP_RARE': 'DIFF_ASDI',
    'DISP_TRRA': 'DIFF_DIFF',
  },
  'POPU_PLCM': {
    'DISP_BANA': 'DIFF_REAU',
    'DISP_TRCO': 'DIFF_REAU',
    'DISP_COUR': 'DIFF_TRFA',
    'DISP_ASCO': 'DIFF_FACI',
    'DISP_INHA': 'DIFF_ASFA',
    'DISP_RARE': 'DIFF_MOYE',
    'DISP_TRRA': 'DIFF_ASDI',
  },
};

const timePopulation = {
  'POPU_MOMI': {
    'DISP_BANA': '1_M',
    'DISP_TRCO': '5_M',
    'DISP_COUR': '10_M',
    'DISP_ASCO': '15_M',
    'DISP_INHA': '30_M',
    'DISP_RARE': '1_H',
    'DISP_TRRA': '1D10_H',
  },
  'POPU_MODM': {
    'DISP_BANA': '5_M',
    'DISP_TRCO': '10_M',
    'DISP_COUR': '15_M',
    'DISP_ASCO': '30_M',
    'DISP_INHA': '1_H',
    'DISP_RARE': '1D10_H',
    'DISP_TRRA': '1D10_J',
  },
  'POPU_MOCM': {
    'DISP_BANA': '10_M',
    'DISP_TRCO': '15_M',
    'DISP_COUR': '30_M',
    'DISP_ASCO': '1_H',
    'DISP_INHA': '1D10_H',
    'DISP_RARE': '1D10_J',
    'DISP_TRRA': '1D10_S',
  },
  'POPU_PLCM': {
    'DISP_BANA': '15_M',
    'DISP_TRCO': '30_M',
    'DISP_COUR': '1_H',
    'DISP_ASCO': '1D10_H',
    'DISP_INHA': '1D10_J',
    'DISP_RARE': '1D10_S',
    'DISP_TRRA': '1D5_MO',
  },
};

const timeMap = {
  M: 'Minutes',
  H: 'Hour(s)',
  J: 'Jour(s)',
  S: 'Semaine(s)',
  MO: 'Mois'
}

function determineTimeValue(timeValue) {
  if (timeValue === '1D10' || timeValue === '1D5') {
    return launchDices(timeValue);
  }
  return timeValue;
}

function determineTime(population, disponibility) {
  const split = timePopulation[population][disponibility].split('_');
  return `${determineTimeValue(split[0])} ${timeMap[split[1]]}`
}


function getDifficultyIdFromDispoPopulation(disponibility, population) {
  return dispoPopulation[population][disponibility]
}

function getDifficulty(difficulties, disponibility, population) {
  return findId(difficulties, getDifficultyIdFromDispoPopulation(disponibility, population));
}

function computeScore(difficulties, socia, disponibility, population) {
  return parseInt(socia) + parseInt(getDifficulty(difficulties, disponibility, population).bonus);
}

function isSuccess(score) {
  return launchD100() <= score;
}

export {
  computeScore,
  determineTime,
  isSuccess
}