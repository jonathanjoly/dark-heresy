export function findId(array, id) {
  return array.find(function (item) {
    return item.id === id;
  });
};
