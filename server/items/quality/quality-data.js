const jsonData = require('../../json/quality.json');
const Quality = require('./quality');

const quality = {
    items: () => jsonData.map(item => new Quality(item.id, item.label, item.modificator, item.priceModificator))
}

module.exports = quality;