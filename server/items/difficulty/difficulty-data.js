const jsonData = require('../../json/difficulty.json');
const Difficulty = require('./difficulty');

const difficulties = {
    items: () => jsonData.map(item => new Difficulty(item.id, item.label, item.bonus))
}


module.exports = difficulties;