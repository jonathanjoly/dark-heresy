class Difficulty {
    constructor(id, label, bonus) {
        this.id = id;
        this.label = label;
        this.bonus = bonus;
    }
}
module.exports = Difficulty;