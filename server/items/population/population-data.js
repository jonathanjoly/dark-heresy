const jsonData = require('../../json/population.json');
const Populations = require('./population');

const populations = {
    items: () => jsonData.map(item => new Populations(item.id, item.label, item.modificator, item.priceModificator))
}
module.exports = populations;