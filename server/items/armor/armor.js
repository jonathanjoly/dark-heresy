class Armor {
    constructor(id, label, Localisations, pa, weight, price, disponibility) {
        this.id = id;
        this.label = label;
        this.Localisations = Localisations;
        this.pa = pa;
        this.weight = weight;
        this.price = price;
        this.disponibility = disponibility;
    }
}


module.exports = Armor;