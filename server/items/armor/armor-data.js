const jsonData = require('../../json/armor.json');
const Armor = require('./armor');

const armors = {
    items: () => jsonData.map(item => new Armor(item.id, item.label, item.bonus))
}



