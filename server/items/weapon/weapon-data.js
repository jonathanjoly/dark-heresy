const jsonData = require('../../json/weapons.json');
const Weapon = require('./weapon');

const weapons = {
    items: () => jsonData.map(item => new Weapon(item.id, item.label, item.group, item.reach, item.mode, item.damage, item.pen, item.at, item.reload, item.weight, item.cost, item.disponibility))
}

module.exports = weapons;

