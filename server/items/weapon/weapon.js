class Weapon {
    constructor(id, label, group, reach, mode, damage, pen, at, reload, weight, cost, disponibility) {
        this.id = id;
        this.label = label;
        this.group = group;
        this.reach = reach;
        this.mode = mode;
        this.damage = damage;
        this.pen = pen;
        this.at = at;
        this.reload = reload;
        this.weight = weight;
        this.cost = cost;
        this.disponibility = disponibility;
    }
}
module.exports = Weapon;