const jsonData = require('../../json/disponibility.json');
const Disponibility = require('./disponibility');

const difficulties = {
    items: () => jsonData.map(item => new Disponibility(item.id, item.label))
}

module.exports = difficulties;