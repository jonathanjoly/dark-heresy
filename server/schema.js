const { buildSchema } = require('graphql');

const schema = buildSchema(`
  type Query {
    language: String
    weapons:[Weapon]
    disponibilties:[Disponibility]
    populations: [Population]
    difficulties: [Difficulty]
    qualities: [Quality]
  }
  type Weapon {
    id: String
    label: String
    group: String
    reach: Float
    mode: String
    dammage: String
    pen: Float
    at: Float
    reload: Float
    weight: Float
    cost: Float
    disponibility: String
  }
  type Disponibility {
    id: String
    label: String
  }
  type Population {
    id: String
    label: String
  }
  type Difficulty {
    id: String
    label: String
    bonus: Float
  }
  type Quality {
    id: String
    label: String
    modificator: Float
    priceModificator: Float
  }
`);

module.exports = schema;