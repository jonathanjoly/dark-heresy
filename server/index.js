const express = require('express');
const graphqlHTTP = require('express-graphql');
const cors = require('cors');
const schema = require('./schema');

const difficulties = require('./items/difficulty/difficulty-data');
const disponibilties = require('./items/disponibility/disponibility-data');
const populations = require('./items/population/population-data');
const qualities = require('./items/quality/quality-data');
const weapons = require('./items/weapon/weapon-data');


const rootValue = {
  language: () => 'GraphQL',

  difficulties: () => difficulties.items(),
  disponibilties: () => disponibilties.items(),
  populations: () => populations.items(),
  qualities: () => qualities.items(),
  weapons: () => weapons.items()
};

const app = express()
app.use(cors())
app.use('/graphql', graphqlHTTP({
  rootValue, schema, graphiql: true
}));
app.listen(4000, () => console.log('Listening on 4000'));